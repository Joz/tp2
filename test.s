			# This code was produced by the CERI Compiler
	.data
FormatString1: .string "%llu\n"
	.align 8
c:	.quad 0
b:	.quad 0
z:	.quad 0
a:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop c
	push $3
	pop b
	push $0
	pop z
FOR1:
	push $7
	pop a
DOWNTO1:
	push b
	push a
	pop %rax
	pop %rbx
	cmpq %rbx, %rax
	je ENDFOR1
DO1:
BEGIN2:
	push c
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop c
	push c
pop %rdx
movq $FormatString1, %rsi
movl $1, %edi
movl $0, %eax
call __printf_chk@PLT
	push z
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop z
END2:
	push a
	pop %rax
	subq $1, %rax
	push %rax
	pop a
	jmp DOWNTO1
ENDFOR1:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
