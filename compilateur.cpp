//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {INT, BOOL,NONE};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string



set<string> DeclaredVariables;
map<string, TYPE> Variables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


TYPE GetVariableType ( string variable)
{
	map<string, TYPE>::iterator itr;
	for( itr = Variables.begin(); itr != Variables.end(); ++itr)
	{
		if( itr->first == variable ) return itr->second;
	}
	Error("Could not find variable");
	return NONE;
}	
		
TYPE Identifier(void){
	string variable = lexer->YYText();
	cout << "\tpush "<<variable<<endl;
	TYPE type = GetVariableType(variable);
	current = (TOKEN) lexer->yylex();
	return type;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE type = INT;
	return type;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else{
		if (current==NUMBER)
			type = Number();
		else
			if(current==ID)
				type = Identifier();
			else
				Error("'(' ou chiffre ou lettre attendue");
	}
	return(type);
	
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	OPMUL mulop;
	TYPE typeTerm = Factor();
	TYPE typeTermBis;
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		typeTermBis = Factor();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if(typeTerm == BOOL && typeTermBis == BOOL){
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# AND"<<endl;	// store result
					break;
				}
			case MUL:
				if(typeTerm == INT && typeTermBis == INT){
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
					break;
				}
			case DIV:
				if(typeTerm == INT && typeTermBis == INT){
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
					break;
				}
			case MOD:
				if(typeTerm == INT && typeTermBis == INT){
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
					cout << "\tpush %rdx\t# MOD"<<endl;		// store result
					break;
				}
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return(typeTerm);
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE typeTerm = Term();
	TYPE typeTerm2;
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		typeTerm2 = Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if(typeTerm == BOOL && typeTerm2 == BOOL){
					cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
					break;	
				}
				else Error("Mêmes types attendus.");
			case ADD:
					if(typeTerm == INT && typeTerm2 == INT){
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					break;
				}
				else Error("Mêmes types attendus.");		
			case SUB:
				if(typeTerm == INT && typeTerm2 == INT){
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					break;
				}
				else Error("Mêmes types attendus.");
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return(typeTerm);

}

void VarDeclaration(void);

//VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){

	if(current != KEYWORD) Error("Un KEYWORD était attendu");
	cout << "\t.data"<<endl;
	cout << "FormatString1: .string \"%llu\\n\"" << endl;
	cout << "\t.align 8" << endl;
	if (strcmp(lexer->YYText(), "VAR") == 0){
		current = (TOKEN) lexer->yylex();
		VarDeclaration();
		while(current == SEMICOLON){
			current = (TOKEN) lexer->yylex();
			VarDeclaration();
		}
	}
	if(current == DOT)current = (TOKEN) lexer->yylex();


}

TYPE getType()
{
	string type = lexer->YYText();

	if( type == "BOOL" ) return BOOL;
	else if( type == "INT" ) return INT;
	Error("Type non reconnu." + type);
	return NONE;
}

// VarDeclaration := Type Ident {"," Ident}
// Type := BOOL | INT
void VarDeclaration(void){
	if(current != TYPEE) Error("Un Type était attendu");
	TYPE Type = getType();
	current = (TOKEN) lexer->yylex();
	string variable = lexer->YYText();

	if(current == ID)current = (TOKEN) lexer->yylex();
	else Error("Expected ID.");
	cout << variable << ":\t.quad 0" << endl;
	DeclaredVariables.insert(variable);
	Variables.insert(pair<string, TYPE>(variable, Type));
	while(current == COMMA){
		current=(TOKEN) lexer->yylex();
		variable = lexer->YYText();
		if(current == ID)current = (TOKEN) lexer->yylex();
		else Error("Expected ID.");
		cout << variable << ":\t.quad 0" << endl;
		DeclaredVariables.insert(variable);
		Variables.insert(pair<string, TYPE>(variable, Type));
	}
}


// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE typeExp = SimpleExpression();
	TYPE typeExpBis;
	TYPE typeRet = BOOL;
	if(current==RELOP){
		oprel=RelationalOperator();
		typeExpBis = SimpleExpression();
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				if(typeExp == typeExpBis){
					cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
					break;
				}
			case DIFF:
				if(typeExp == typeExpBis){
					cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
					break;
				}
			case SUPE:
				if(typeExp == typeExpBis){
					cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
					break;
				}
			case INFE:
				if(typeExp == typeExpBis){
					cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
					break;
				}
			case INF:
				if(typeExp == typeExpBis){
					cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
					break;
				}
			case SUP:
				if(typeExp == typeExpBis){
					cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
					break;
				}
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return(typeRet);
	}
	return(typeExp);
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	string variable;
	TYPE typeExp;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	typeExp = Expression();
	if(typeExp == GetVariableType(variable))cout << "\tpop "<<variable<<endl;
	else Error("Types incompatibles.");
	return(variable);
}

void Statement(void);

// IFStatement := "IF" Expression "THEN" Statement {"ELIF" Expression "THEN" Statement} ["ELSE" Statement]

void IfStatement(void){
	TYPE typeExp;
	int numTag = ++TagNumber;
	int cpt = 0;
	if (current!=KEYWORD) Error("Keyword attendu.");
	
	if (strcmp(lexer->YYText(), "IF") == 0){
		cout << "IF" << numTag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		typeExp = Expression();
		if(!(typeExp == BOOL)) Error("Expected type BOOL.");
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tjz ELIFBLOCK" << numTag << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
		{
			cout << "THEN" << numTag << ":" << endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp ENDIF" << numTag << endl;
			cout << "ELIFBLOCK" << numTag << ":" << endl;
			while(current == KEYWORD && strcmp(lexer->YYText(), "ELIF") == 0)
			{
				cout << "ELIF" << numTag << cpt << ":" << endl;
				current = (TOKEN) lexer->yylex();
				typeExp = Expression();
				if(!(typeExp == BOOL)) Error("Expected type BOOL.");
				cout << "\tpop %rax" << endl;
				cout << "\tcmpq $0, %rax" << endl;
				cout << "\tjz ENDELIF" << numTag << cpt << endl;
				if(current == KEYWORD && strcmp(lexer->YYText(), "THEN") == 0)
				{
					cout << "THEN" << numTag << cpt << ":" << endl;
					current=(TOKEN) lexer->yylex();
					Statement();
					cout << "\tjmp ENDIF" << numTag << endl;

				}

				cout << "ENDELIF" << numTag << cpt << ":" << endl;
				cpt++;

			}
			cout << "ELSE" << numTag << ":" << endl;
			if(current == KEYWORD && strcmp(lexer->YYText(), "ELSE") == 0)
			{
				
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}
		else Error("Expected THEN");
	}
	
	else Error("Expected IF");
	
	cout << "ENDIF" << numTag << ":" << endl;
}


//SwitchStatement := "SWITCH" Expression {"CASE" Expression Statement}

void SwitchStatement(void){
	TYPE typeExp;
	TYPE typeExp2;
	int numTag = ++TagNumber;
	int cpt = 0;
	if (current != KEYWORD) Error("Keyword attendu.");

	if (strcmp(lexer->YYText(), "SWITCH") == 0){
		cout << "SWITCH" << numTag << ":" << endl;
		current = (TOKEN) lexer->yylex();
		Expression();
		cout << "\tpop %rax" << endl;

		while(current == KEYWORD &&  strcmp(lexer->YYText(), "CASE") == 0)
		{
			current = (TOKEN) lexer->yylex();
			Expression();

			//if (typeExp2 != typeExp) Error("Types Switch/Case incompatibles.");

			cout << "\tpop %rbx" << endl;
			cout << "\tcmpq %rax, %rbx" << endl;
			cout << "\tjnz ENDCASE" << numTag << cpt << endl;

			Statement();
		
			cout << "ENDCASE" << numTag << cpt << ":" << endl;
			cpt++;
		}

	}

}

//WhileStatement := "WHILE" Expression "DO" Statement

void WhileStatement(void){
	TYPE typeExp;
	int numTag = ++TagNumber;
	if (current!=KEYWORD) Error("Keyword attendu.");
	
	if (strcmp(lexer->YYText(), "WHILE") == 0){
		cout << "WHILE" << numTag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		typeExp  = Expression();
		if(!(typeExp == BOOL)) Error("Type BOOL attendu.");
		cout << "\tpop %rax" << endl;
		cout << "\tcmpq $0, %rax" << endl;
		cout << "\tjz ENDWHILE" << numTag << endl;
		if(current == KEYWORD && strcmp(lexer->YYText(), "DO") == 0)
		{
			cout << "DO" << numTag << ":" << endl;
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp WHILE" << numTag<< endl;
			
		}
		else Error("Expected DO");
		cout << "ENDWHILE" << numTag << ":" << endl;
	}
}

//ForStatement := "FOR" AssignementStatement ("TO"|"DOWNTO") Expression "DO" Statement

void ForStatement(void){
	int numTag = ++TagNumber;
	if (current!=KEYWORD) Error("Keyword attendu.");
	
	if (strcmp(lexer->YYText(), "FOR") == 0){
		cout << "FOR" << numTag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		string var = AssignementStatement();
		string incr = lexer->YYText();
		if(incr == "TO" || incr == "DOWNTO")
		{
			if(incr == "TO") cout << "TO" << numTag << ":" << endl;
			else cout << "DOWNTO" << numTag << ":" << endl;
			current=(TOKEN) lexer->yylex();
			Expression();
			cout << "\tpush " << var << endl;
			cout << "\tpop %rax" << endl;
			cout << "\tpop %rbx" << endl;
			cout << "\tcmpq %rbx, %rax" << endl;
			cout << "\tje ENDFOR" << numTag << endl;
			if(strcmp(lexer->YYText(), "DO") == 0)
			{
				cout << "DO" << numTag << ":" << endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				cout << "\tpush " << var << endl;
				cout << "\tpop %rax" << endl;
				if(incr == "TO") cout << "\taddq $1, %rax" << endl;
				else cout << "\tsubq $1, %rax" << endl;
				cout << "\tpush %rax" << endl;
				cout << "\tpop " << var << endl;
				if(incr == "TO") cout << "\tjmp TO" << numTag << endl;
				else cout << "\tjmp DOWNTO" << numTag << endl;
			}
			else Error("Expected DO");
		}
		else Error("Expected TO or DOWNTO");
		cout << "ENDFOR" << numTag << ":" << endl;
	}
}

// BlockStatement := "BEGIN" Statement {";" Statement} "END"

void BlockStatement(void){
	int numTag = ++TagNumber;
	if (current!=KEYWORD) Error("Keyword attendu.");
	
	if (strcmp(lexer->YYText(), "BEGIN") == 0){
		cout << "BEGIN" << numTag << ":" << endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		
		while(strcmp(lexer->YYText(), ";") == 0){
			current=(TOKEN) lexer->yylex();
			Statement();			
		}
		if (strcmp(lexer->YYText(), "END") == 0){
			cout << "END" << numTag << ":" << endl;
			current=(TOKEN) lexer->yylex();
		}
		else{
			Error("Expected end.");
		}
	}
		
	else{
		Error("Expected begin.");
	}
}

// Display := "DISPLAY" Expression

void Display(void){
	if (current != KEYWORD) Error("Keyword attendu.");

	if (strcmp(lexer->YYText(), "DISPLAY") == 0){
		current=(TOKEN) lexer->yylex();
		Expression();
		cout << "pop %rdx" << endl;
		cout << "movq $FormatString1, %rsi" << endl;
		cout << "movl $1, %edi" << endl;
		cout << "movl $0, %eax" << endl;
		cout << "call __printf_chk@PLT" << endl;
	}
}



// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void){
	if( strcmp(lexer->YYText(), "IF") == 0) IfStatement();
	else if( strcmp(lexer->YYText(), "WHILE") == 0) WhileStatement();
	else if( strcmp(lexer->YYText(), "FOR") == 0) ForStatement();
	else if( strcmp(lexer->YYText(), "BEGIN") == 0) BlockStatement();
	else if ( strcmp(lexer->YYText(), "DISPLAY") == 0) Display();
	else if (strcmp(lexer->YYText(), "SWITCH") == 0) SwitchStatement();
	else if( current == ID ) AssignementStatement();
	else Error("Expected Statement.");
}




// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





